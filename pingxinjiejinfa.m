M=0.5;%飞行速度的马赫数；
Vsp=340;%空气中的声速；
V0=M*Vsp;
Vd=3.8*V0;%导弹的飞行速度；
Vm=1.5*V0;%目标的飞行速度；???
sgmam=60;%假设目标的速度矢量与基准线的夹角为60度；
q=90;%假设目标线与基准之间的夹角为90度，保持不变；
ata=asin(Vm*sin(q-sgmam)/Vd);%?一阶线性微分方程组；
fun=@(t,x)[Vm*cos((q-sgmam)*pi/180)-Vd*cos(ata);0;0];
x0=[3000.0,90,0];%求解初始条件；
[t,y]=ode45(fun,[0,8.0],x0);%求解微分方程组；
%?导弹与目标之间的距离r的变化情况；
figure(1);plot(t,y(:,1),'r','LineWidth',1);grid on;
xlabel('时间/s');ylabel('导弹与目标之间的距离r/m');
title('平行接近法');%?目标线与基准线之间夹角q的变化情况；
figure(2);plot(t,y(:,2),'r','LineWidth',1); grid on;
xlabel('时间/s');ylabel('目标线与基准线之间的夹角q');
title('平行接近法');%?目标线与基准线之间夹角的角速率变化情况；
figure(3);plot(t,y(:,3),'r','LineWidth',1);grid on;
xlabel('时间/s');ylabel('目标线与基准线之间夹角的角速率');
title('平行接近法');