M=0.5;%飞行速度的马赫数；
Vsp=340;%空气中的声速；
V0=M*Vsp;
Vd=3.8*V0;%导弹的飞行速度；
Vm=1.5*V0;%目标的飞行速度；
sgmam=60;%假设目标的速度矢量与基准线的夹角为60度；
K=2.0;%?一阶线性微分方程组；
fun=@(t,x)[Vm*cos(x(3)*pi/180)-Vd*cos(x(4)*pi/180);
    (Vd*sin(x(4)*pi/180)-Vm*sin(x(3)*pi/180))/x(1);
    (Vd*sin(x(4)*pi/180)-Vm*sin(x(3)*pi/180))/x(1);
    (1-K)*(Vd*sin(x(4)*pi/180)-Vm*sin(x(3)*pi/180))/x(1)];
x0=[3000.0;90.0;30;40];%求解初始条件；
[t,y]=ode45(fun,[0,10.6],x0);%求解微分方程组；
% 导弹与目标之间的距离r的变化情况；
figure(1);
plot(t,y(:,1),'r','LineWidth',1); grid on;
xlabel('时间/s');
ylabel('导弹与目标之间的距离r/m');
title('比例导引法');% 目标线与基准线之间夹角q的变化情况； 
figure(2);
plot(t,y(:,2),'r','LineWidth',1); grid on;
xlabel('时间/s');
ylabel('目标线与基准线之间的夹角q'); 
title('比例导引法');   % 目标速度矢量与目标线之间夹角的变化情况；
figure(3);
plot(t,y(:,3),'r','LineWidth',1); grid on;
xlabel('时间/s');
ylabel('目标速度矢量与目标线之间夹角'); 
title('比例导引法');   % 导弹速度矢量与目标线之间夹角的变化情况； 
figure(4);
plot(t,y(:,4),'r','LineWidth',1);grid on;
xlabel('时间/s');
ylabel('导弹速度矢量与目标线之间夹角');
title('比例导引法');
s=y(:,2);
T=t;
N=length(s);
M=length(T);
figure(5);
yq=zeros(1,N-1);
Tq=zeros(1,N-1);
for k=1:N-15
    Tq(k)=T(k);
    yq(k)=(s(k+1)-s(k))/(T(k+1)-T(k));
end
plot(Tq,yq,'r*','LineWidth',2);grid on;
xlabel('时间/s');
ylabel('目标线与基准线之间的夹角q的变化率');
title('比例导引法');