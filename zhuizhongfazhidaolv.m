M=0.5;%飞行速度的马赫数M=0.5；?
Vsp=340;%空气中的声速；
Vm=M*Vsp;
sgmam=80;%假设目标的速度矢量与基准线的夹角为80度；
%一阶线性微分方程组；
fun=@(t,x)[1.5*Vm*cos((x(2)-sgmam)*pi/180)-3.8*Vm;
    (-1.5*Vm*sin((x(2)-sgmam)*pi/180))/x(1);
    (-1.5*Vm*sin((x(2)-sgmam)*pi/180))/x(1)];
x0=[3000.0;90.0;90.0];%求解初始条件；??
[t,y]=ode45(fun,[0,7.6],x0);%求解微分方程组；%?导弹与目标之间的距离r的变化情况；
figure(1);
plot(t,y(:,1),'r','LineWidth',1);
grid on;
xlabel('时间/s');
ylabel('导弹与目标之间的距离r/m');
title('追踪法引导律');%?目标线与基准线之间夹角q的变化情况；
figure(2);
plot(t,y(:,2),'r','LineWidth',1);
grid on;
xlabel('时间/s');
ylabel('目标线与基准线之间的夹角q');
title('追踪法引导律');
%?导弹速度矢量与基准线之间的夹角sgma变化情况；
figure(3);
plot(t,y(:,3),'r','LineWidth',1);
grid on;
xlabel('时间/s');
ylabel('导弹速度矢量与基准线之间的夹角sgma');
title('追踪法引导律');%?目标线与基准线之间的角速率的变化情况；
s=y(:,2);
T=t;
N=length(s);
M=length(T);
figure(4);
yq=zeros(1,N-1);
Tq=zeros(1,N-1);
for k=1:N-15
    Tq(k)=T(k);
    yq(k)=(s(k+1)-s(k))/(T(k+1)-T(k));
end
plot(Tq, yq, 'r*', 'LineWidth', 1);
grid on;
xlabel('时间/s');
ylabel('目标线与基准线之间的夹角q的变化率');
title('追踪法引导律');